#!/bin/sh
# This script is used by systemd /lib/systemd/system/centrifydc.service.
# Use this script because systemd service file does not support conditional
# options below.
. /etc/centrifydc/scripts/functions.cdc

# start adclient and wait until connected mode or 60s timeout
$DAEMON $OPTIONS $STARTUP_OPTIONS && \
    wait_adclient
    