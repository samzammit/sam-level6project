# MCAST Year 2 Level 6 Project - GIT Repository
---
**Samuel Markus Zammit CSN 6.2A**

**Research Title:** Identifying Group Policy setbacks while implementing Active Directory cross-platform

## File Structure
---
Root

- **src**
    * Centrify DirectManage Express
    * Centrify Infrastructure Services
    * PBIS Open
    * Likewise Open (OSS)
  
- **lit**
    * Active Directory and Related Aspects of Security. 2018 21st Saudi Computer Society National Computer Conference (NCC)..pdf
    * Centeris - UNIX.pdf
    * Comparing centrify versus likewise open and windbind.pdf
    * Directory Services for Linux in comparison with NDS and Microsoft Active Directory.pdf
  
- **doc**
    * Initial Proposal
    * IEEE Paper
    * Poster
    * Logbook

---

Should you have any enquiries, feel free to contact me by email at samuel.zammit.a100206@mcast.edu.mt :)
